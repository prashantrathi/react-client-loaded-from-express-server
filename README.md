# README #

This repo is created to define how react auth client(auth-client-react-and-redux) and express auth server(auth-server-express-and-mongodb) will run on the single port.
In this express server will load the react client from the single bundle.js file created using Webpack configuration.

### Running React and Redux client from the Express and MongoDB server ###

* Clone server repo using git clone command in "https://bitbucket.org/prashantrathi/auth-server-express-and-mongodb"
* cd auth-server-express-and-mongodb
* Clone client repo using git clone command in "https://bitbucket.org/prashantrathi/auth-client-react-and-redux"

### Structure here is:
```
|server
	|client
		|package.json
		|webpack.config.js
		|index.html
		|other client files
	|package.json
	|index.js
	|other server files
```
# CLIENT

### STEPS TO FOLLOW:
#### (Below steps are very much specific for directory and files organization as explained in above structure)

1) Create "dist" directory in client repo
2) npm install --save webpack (if webpack node module is not yet installed)

### Modify client package.json "scripts" object
2) Add postinstall script to create production version of complete ".js" file.
	
* Add "postinstall":"node ./node_module/webpack/bin/webpack.js -p && NODE_ENV=production && cp -v ./index.html ./dist/index.html && cp -v ./style/style.css ./dist/style.css"

	(This command does following)
	
	1) node ./node_module/webpack/bin/webpack.js -p: Creates the production ready bundle.js
	2) cp -v ./index.html ./dist/index.html: copy the index.html to dist directory
	3) cp -v ./style/style.css ./dist/style.css: copy the style.css to dist directory.
	

* Add "server":"node ../index.js"
  (This will heelp to run the server)
	
### Modify client webpack.config.js
3) npm install --save path (path node module)

4) modify webpack.config.js to have following structure

```
const path = require('path');

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname,"dist"),
    publicPath: '/'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
         presets: ['react', 'es2015', 'stage-1']
        }
     }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './dist'
  }
};
```


# SERVER

### STEPS TO FOLLOW:

1) npm install --save path

### Modify server index.js to load react bundle.js static file from the dist directory:
1) Add
 
	const path = require('path');

	const DIST_DIR = path.join(__dirname, "auth-client-react-and-redux/dist");
	
	app.use(express.static(DIST_DIR));

# Awesome Its done
* Open terminal
* cd to client directory
* npm postinstall 
* npm run server

Go to the browser and type url "localhost:3090". This should load the React client from the Express server.

#### TODO: 
Need to understand using webpack.config commands to move files like html and use css loaders to define dist folder structure at runtime. 
This will help to remove package.json commands to copy index.html and style.css and define dynamic folder structure based on requirement.

* >Issue: You may see style.css is not getting applied. 
* >Fix: Before running "npm postinstall" change index.html 
```
<link rel="stylesheet" href="/style/style.css"> 
to 
<link rel="stylesheet" href="style.css">
```
